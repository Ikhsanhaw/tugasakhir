import face_recognition
import sys
# import dlib
import fnmatch
import os
import random
import re
import numpy as np
# import requests

def deletefile(name):
	filename = os.listdir("data/encoding/")
	filename2 = os.listdir("data/images/")
	for file in filename:
		if fnmatch.fnmatch(file,'*'+name+'.npy'):
			os.remove('data/encoding/'+file)
	for file in filename2:
		if fnmatch.fnmatch(file,'*'+name+'.jpeg'):
			os.remove('data/images/'+file)
	# os.remove("data/encoding/")
def ceknumoffile():
    with open("data/numoffile.dat","rb") as f:
        numoffile = f.read(1)
    return int(numoffile)
def writenumoffile():
	number = ceknumoffile()
	newnumber = number + 1
	with open("data/numoffile.dat","w+") as f:
		f.write(str(newnumber))

def updatefile(name,newname):
	filename = os.listdir("data/encoding")
	for file in filename:
		if fnmatch.fnmatch(file,'*'+name+'.npy'):
			os.chdir("data/encoding")
			nomor = str(file).split('_')[0]
			os.rename(file,nomor+'_'+newname+'.npy')
			writenumoffile()
			# print(file)
name = sys.argv[1]
# newname = sys.argv[2]

deletefile(name)


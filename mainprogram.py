import face_recognition
import cv2
import os
import re
import requests
import json
import time
import csv
import numpy as np
from AutoSight.BotUtil import Bot_Util
# from pymessenger.bot import Bot
# bot = Bot("EAAbQK2XfXwcBAIZAJ2lXrLJ17mHfAZC6mZCY6fhZC3lOYKuNk06s27hdZBwbXflElYnMnPZAzmXO0sJSE5VSolkWIQjJA8xM1zc0KEDoYMtkccwkq0W2FrAMYGbMpaQJSUfuIusVlIni9p56qNCLaj2nNCXQI2UwI0TG8GkZCY8mQZDZD")

def loadfile():
    known_face_names = []
    known_face_encodings = []
    listfile = os.listdir("data/encoding/")
    for file in listfile:
        arrayencoding = np.load("data/encoding/"+file)
        known_face_encodings.append(arrayencoding)
        regex = re.search('_(.*?).npy',file).group(1)
        known_face_names.append(regex)
    return known_face_names,known_face_encodings

def ceknumoffile():
    with open("data/numoffile.dat","rb") as f:
        numoffile = f.read(1)
    return int(numoffile)
def writenumoffile(a):
    with open("data/numoffile.dat",'w') as f:
        f.write(a)
    print("update numoffile selesai")
    return 0

video_capture = cv2.VideoCapture(0)

print video_capture.get(3)
print video_capture.get(4)

known_face_names,known_face_encodings= loadfile()

bot=Bot_Util()

face_locations = []
face_encodings = []
face_names = []
process_this_frame = False
counter = 0
framecurl = 0
while True:
    listfile = os.listdir("data/encoding/")
    numoffile = ceknumoffile()
    print(numoffile)
    if(numoffile!=len(listfile)):
        known_face_names,known_face_encodings= loadfile()
        writenumoffile(str(len(listfile)))

    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing
    # resized = cv2.resize(frame, (1280,720))
    # small_frame = cv2.flip(resized,-1)
    small_frame = frame
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]
    if(counter%10==0):
        process_this_frame= True
    else:
        process_this_frame= False

    # Only process every other frame of video to save time
    # penghitung =0
   
    if process_this_frame:
        arraynilai = []
        penghitung = 0
        print('frame '+ str(counter) +' diproses')
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        # print(face_locations)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
        #print(face_encodings)
        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding, 0.55)
            nilai = face_recognition.face_distance(known_face_encodings,face_encoding)
            print "nilaike ",penghitung," adalah ",nilai[0]
            # print(type(nilai)
            # print("nilaiminimal" ,min(arraynilai))
            penghitung = penghitung+1
            # print(matches)
            name = "Unknown"
            # If a match was found in known_face_encodings, just use the first one.
            if True in matches:
                arraynilai = nilai.tolist()
                # indexnilai = nilai.where(nilai,min(nilai))
                indexnilai = arraynilai.index(min(arraynilai))
                print(indexnilai)
                if(matches[indexnilai]==True):
                    name = known_face_names[indexnilai]
                # first_match_index = matches.index(True)
                # name = known_face_names[first_match_index]
                framecurl = 1

                # print("gajadi curl")
            else:
                framecurl += 1
                if(framecurl%5==0):
                    timenow = str(time.ctime())
                    filename = 'unknown '+timenow+'.jpg'
                    filepath = 'data/images/unknown/'
                    # filepath2 = 'data/images/'
                    cv2.imwrite(filepath+"unknown.jpeg",small_frame)
                    # cv2.imwrite(filepath2+"unknown.jpeg",small_frame)
                    print("write image selesai")
                    print("tinggal kirim foto")
                    bot.send_report("now","unknown.jpeg")
                    # bot.send_image_url("1846098732115223","https://splendid-bird-53.localtunnel.me/images/unknown.png")
                    # bot.send_text_message("1846098732115223", "ada yang gak dikenalin nih di jam "+timenow)
                    # bot.send_report("1846098732115223",timenow,"https://splendid-bird-53.localtunnel.me/images/unknown.png")
                    # sleep()
                    # bot.send_image_url("1846098732115223", "https://splendid-bird-53.localtunnel.me/images/unknown.png")
                    print("kirim foto selesai")
                    framecurl = 0
                    # send()
                    print("curl disini ")

            face_names.append(name)

    # process_this_frame = not process_this_frame
    # print('frame '+str(counter)+' tidak diproses')
    counter += 1
    

    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        # top *= 4
        # right *= 4
        # bottom *= 4
        # left *= 4

        # Draw a box around the face
        cv2.rectangle(small_frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(small_frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(small_frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', small_frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam

video_capture.release()
cv2.destroyAllWindows()
